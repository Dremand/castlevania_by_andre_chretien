﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{

    // Used to keep track of projectile life
    public float lifeTime;

    // Used to control projectile speed
    public float speed;

    // Use this for initialization
    void Start()
    {

        // Check if lifeTime variable was set to something not 0
        if (lifeTime <= 0)
        {
            //Print message that lifeTime was not set.
            // - Default to a value
            Debug.LogWarning("lifeTime variable was not set in Inspector.");

            //Defult to a value of 1 second
            lifeTime = 1.0f;
        }

        // Delete gameObject Script is attached to
        Destroy(gameObject, lifeTime); //no lifeTime makes it destroy right away

        // Tell Projectile to move using speed variable
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
        //GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);

    }

    // Update is called once per frame
    void Update()
    {


    }

    void OnCollisionEnter2D(Collision2D coll)
    {
         if (gameObject.tag == "Bullet") 
        //Destroy(gameObject);
        Debug.LogWarning("Stopbullet");
    }
}
