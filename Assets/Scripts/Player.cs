﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    // Version 1: Reference Rigidbody through Script
    Rigidbody2D rb; // Declares a private variable not shown in Inspector
    // Version 2: Reference Rigidbody through Inspector or Script
    public Rigidbody2D rb2; // Declares a private variable not shown in Inspector

    //Used for Character movement
    // - public so it can be changed through inspector while in playmode

    public float speed;

    // Used for Character jump
    public float jumpForce;
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public bool shoot;

    //public bool doubleJump;

    //Used for animation of character
    Animator anim;

    // Used for Projectile firing
    public Transform projectileSpawnPoint;
    public Projectile projectile;

    // Used for flipping Character
    public bool isFacingLeft;

    // Use this for initialization
    //oncollisionenter or ontriggerenter
    void Start()
    {

        //Version 1: Reference Rigidbody through Script
        rb = GetComponent<Rigidbody2D>();
        // Check is Rigidbody2d is attached to GameObject Script is attached to script
        if (!rb)
        {
            //Print meesage to Console saying Rigidbody2D not attached
            Debug.LogWarning("No Rigidbody2D found on GameObject.");
        }
        // Check is Rigidbody2d is attached to GameObject Script is attached to script
        if (!rb2)
        {
            //Print meesage to Console saying Rigidbody2D not attached
            Debug.LogWarning("rb2: No Rigidbody2D found on GameObject.");
        }

        if (speed <= 0) // or <= 0.9f
        {
            //Print message to console saying speed was not set
            Debug.LogWarning("Speed variable not set. Setting to defult value");

            //Assign a default value if Speed not set
            speed = 5.0f;
        }
        // Check to ensure JumpForce is set to a value not 0
        if (jumpForce <= 0.9f)
        {
            //Print message to console saying JumpForce was not set
            Debug.LogWarning("JumpForce variable not set. Setting to default value");

            //Assign a default value if JumpForce not set
            jumpForce = 5.0f;
        }
        anim = GetComponent<Animator>();
        if (!anim)
        {
            // Check
            Debug.LogWarning("No Animator found on GameObject.");
        }
        //Check if SpawnPoint was connected to Script
        if (!projectileSpawnPoint)
        {
            //Print messsage to console saying SpawnPoint was not connected
            Debug.LogWarning("No SpawnPoint found.");
        }
        {
            //Print messsage to console saying Projectile Prefab was not connected
            Debug.LogWarning("No Projectile Prefab found.");
        }
    }

    // Update is called once per frame
    void Update()
    {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.1f, isGroundLayer);
        //check out different ones like overlap all instead of overlap circle, 
        //change Radius 0.1f to varible

        float moveValue = Input.GetAxis("Horizontal");
        //float moveValue2 = Input.GetAxis("Horizontal");

        // Check if 'Jump' (Space unless changed) Button was pressed
        if (Input.GetButtonDown("Jump") && isGrounded) // <-- add in || doubleJump
        {
            // 'Jump' (Space unless changed) was pressed. So jump.

            //Applies a force in Up direction
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            //play around using force instead of Impulse
        }

        // Check if a spacific key was pressed
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("pew pew!");
            //Create Projectile for firing
            // - Need prefab to instantiate(create)
            // - Need location to instantiate projectile at
            // - Need rotation to instantiate projectile with
            Projectile temp = Instantiate(projectile, projectileSpawnPoint.position,
                projectileSpawnPoint.rotation) as Projectile;

            temp.speed = transform.localScale.x * -7.0f;

        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            anim.SetBool("Fire", shoot = true);


        }

        else
        {
            anim.SetBool("Fire", shoot = false);
        }
/*
        if (Input.GetKeyDown(KeyCode.S) || (Input.GetKeyDown(KeyCode.W)))
        {
            anim.SetBool("Climb", shoot = true);
            //Debug.Log("pew pew!");
        }

        else
        {
            anim.SetBool("JumpFire", isGrounded); anim.SetBool("Climb", shoot = false);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            anim.SetBool("JumpFire", shoot = true);
            //Debug.Log("pew pew!");
        }

        else
        {
            anim.SetBool("JumpFire", shoot = false);
        }
        */
        //Move character using Rigidbody2D's velocity varible
        // -Uses value from GetAxis stored in moveValue to move Character
        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);

        anim.SetFloat("Speed", Mathf.Abs(moveValue));

        anim.SetBool("Grounded", isGrounded);

        if (moveValue > 0 && isFacingLeft)
            flipCharacter();
        else if (moveValue < 0 && !isFacingLeft)
            flipCharacter();

        //anim.SetBool("RunFire", Mathf.Abs(moveValue2));
    }
    void flipCharacter()
    {
        // Version 1: Toggle isFacingLeft variable
        isFacingLeft = !isFacingLeft;

        // Version 2: Toggle isFacingLeft variable
        /* if (isFacingLeft)
             isFacingLeft = false;
         else
             isFacingLeft = true;*/
        //Version 3
        //isFacingLeft?isFacingLeft=false:isFacingLeft=true;

        // Make a copy of old scale
        Vector3 scaleFactor = transform.localScale;

        // Flip scale of x
        scaleFactor.x *= -1; // or scaleFactor.x = -scaleFactor.x

        //Update scale to new flipped scale
        transform.localScale = scaleFactor;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bullet")
            coll.gameObject.SendMessage("ApplyDamage", 10);
        //  Destroy(gameObject);
    }
}